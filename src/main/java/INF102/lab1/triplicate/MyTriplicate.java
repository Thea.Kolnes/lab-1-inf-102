package INF102.lab1.triplicate;

import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {
    
    @Override
    public T findTriplicate(List<T> list) {

        for (int i = 0; i < list.size(); i++) {
            for (int j = 0; j < i; j++) {
                if (list.get(i).equals(list.get(j))) {
                    for (int z = 0; z < j; z++) {
                        if (list.get(i).equals(list.get(z))) {
                            return list.get(z);
                        }
                    }
                }
            }
        }
        return null;
    }
}
